# frozen_string_literal: true

class FileInputsController < ApplicationController
  before_action :find_file_input, only: %i[show destroy]

  def index
    @file_inputs = FileInput.all.includes(:purchases)
  end

  def show; end

  def new
    @file_input = FileInput.new
  end

  def create # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
    file = params[:file_input][:file]
    params[:file_input].delete(:file)

    @file_input = FileInput.new(file_input_params)

    respond_to do |format|
      if @file_input.save
        FileInput.process_purchases(file, @file_input.id)

        format.html { redirect_to file_input_url(@file_input), notice: I18n.t(:success_file_insert) }
        format.json { render :show, status: :created, location: @file_input }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @file_input.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @file_input.destroy

    respond_to do |format|
      format.html { redirect_to file_inputs_url, notice: I18n.t(:success_file_destroy) }
      format.json { head :no_content }
    end
  end

  private

  def find_file_input
    @file_input = FileInput.find(params[:id])
  end

  def file_input_params
    params.fetch(:file_input, {})
  end
end
