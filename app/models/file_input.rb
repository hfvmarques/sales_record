# frozen_string_literal: true

require_relative '../../lib/file_parser'

class FileInput < ApplicationRecord
  has_many :purchases, dependent: :destroy

  def purchases
    @purchases = Purchase.where(file_input_id: id).includes(:purchaser, :item, :merchant)
  end

  def gross_income
    purchases.includes(:item).map(&:income).sum
  end

  def self.process_purchases(file, file_input_id)
    sales = FileParser.new(file).content
    sales.each do |sale|
      params = set_params(sale, file_input_id)

      Purchase.register(params)
    end
  end

  def self.set_params(sale_data, file_input_id)
    {
      purchaser_name: sale_data[0],
      item_description: sale_data[1],
      item_price: sale_data[2].to_f,
      purchase_count: sale_data[3].to_i,
      merchant_address: sale_data[4],
      merchant_name: sale_data[5],
      file_input_id:
    }
  end

  private_class_method :set_params
end
