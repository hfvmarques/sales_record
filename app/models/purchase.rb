# frozen_string_literal: true

class Purchase < ApplicationRecord
  belongs_to :purchaser
  belongs_to :item
  belongs_to :merchant
  belongs_to :file_input

  validates :count, presence: true

  def income
    (item.price * count)
  end

  def self.register(params)
    create(
      purchaser: Purchaser.find_or_create(params),
      item: Item.find_or_create(params),
      count: params[:purchase_count],
      merchant: Merchant.find_or_create(params),
      file_input: FileInput.find(params[:file_input_id])
    )
  end

  def self.total_gross_income
    all.includes(:item).map do |purchase|
      (purchase.item.price * purchase.count)
    end.sum
  end
end
