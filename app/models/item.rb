# frozen_string_literal: true

class Item < ApplicationRecord
  validates :description, presence: true
  validates :price_in_cents, presence: true

  has_many :purchases

  def price
    (price_in_cents.to_f / 100)
  end

  def self.find_or_create(params)
    items = where(description: params[:item_description])
    params_price = (params[:item_price] * 100).to_i

    return Item.create(description: params[:item_description], price_in_cents: params_price) if items.empty?

    items.map do |item|
      return item if params_price == item.price_in_cents

      Item.create(description: params[:item_description], price_in_cents: params_price)
    end.first
  end
end
