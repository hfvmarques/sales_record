# frozen_string_literal: true

class Purchaser < ApplicationRecord
  validates :name, presence: true

  has_many :purchases

  def self.find_or_create(params)
    find_by(name: params[:purchaser_name]) || create(name: params[:purchaser_name])
  end
end
