# frozen_string_literal: true

class Merchant < ApplicationRecord
  validates :name, presence: true
  validates :address, presence: true

  has_many :purchases

  def self.find_or_create(params)
    find_by(name: params[:merchant_name]) || create(
      name: params[:merchant_name],
      address: params[:merchant_address]
    )
  end
end
