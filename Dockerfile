FROM ruby:3.1.2-alpine AS base
RUN apk add --update --no-cache \
  build-base \
  sqlite-dev \
  curl \
  git \
  postgresql-dev \
  bash \
  yarn \
  tzdata \
  icu-data-full
WORKDIR /app
COPY Gemfile Gemfile.lock ./
RUN bundle install
COPY . ./

CMD ["./entrypoints/docker-entrypoint.sh"]