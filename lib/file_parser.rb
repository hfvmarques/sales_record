# frozen_string_literal: true

require 'json'

class FileParser
  attr_accessor :file, :content

  HEADER_POSITION = 0

  def initialize(file)
    @file = file
    @content = set_content.drop(HEADER_POSITION + 1)
  end

  def headers
    set_content[HEADER_POSITION]
  end

  def set_content
    read_file = File.open(file)
    content = read_file.map { |line| line.strip.split("\t") }

    content.map { |line| line.map { |attr| attr.gsub('"', '') } }
  end
end
