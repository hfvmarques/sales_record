# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_10_25_182231) do
  create_table "file_inputs", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "items", force: :cascade do |t|
    t.string "description", null: false
    t.integer "price_in_cents", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "merchants", force: :cascade do |t|
    t.string "name", null: false
    t.string "address", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "purchasers", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "purchases", force: :cascade do |t|
    t.integer "purchaser_id", null: false
    t.integer "item_id", null: false
    t.integer "count"
    t.integer "merchant_id", null: false
    t.integer "file_input_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["file_input_id"], name: "index_purchases_on_file_input_id"
    t.index ["item_id"], name: "index_purchases_on_item_id"
    t.index ["merchant_id"], name: "index_purchases_on_merchant_id"
    t.index ["purchaser_id"], name: "index_purchases_on_purchaser_id"
  end

  add_foreign_key "purchases", "file_inputs"
  add_foreign_key "purchases", "items"
  add_foreign_key "purchases", "merchants"
  add_foreign_key "purchases", "purchasers"
end
