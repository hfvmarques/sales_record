class CreateItems < ActiveRecord::Migration[7.0]
  def change
    create_table :items do |t|
      t.string :description, null: false
      t.integer :price_in_cents, null: false

      t.timestamps
    end
  end
end
