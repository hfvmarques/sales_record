# Sales Record

### MVC WebApp built in Ruby on Rails for sales management.

The base repository is [this](https://gitlab.com/nuuvem-public/tests/web-1).

## Versions

- Ruby 3.1.2
- Rails 7.0.4

## Production

This app is running in production mode [here](https://sales-record-test.herokuapp.com/).

## Development environment

There are two ways to run this project, on a docker container or locally.

### Docker container

1. First of all, you must have [docker](https://www.docker.com/) daemon and [docker-compose](https://docs.docker.com/compose/install/) installed on your computer;
2. Next you want to build the image. To do that you must run the command bellow in that project root folder:

```
docker-compose build
```

3. Then to start puma, use the command bellow:

```
docker-compose up
```

Or if you want to keep the terminal usable:

```
docker-compose up -d
```

4. Before opening the browser or running any of the tests, you must first migrate the schema, and for that run:

```
docker-compose exec app rails db:migrate
```

5. Now you may use the webapp at `http://localhost:3000`.

### Locally

1. First you must install ruby. I prefer the [asdf](https://asdf-vm.com/) to do that. I suggest you follow their tutorial on how to install it;
2. Then you must add the ruby plugin to asdf like they explain [here](https://github.com/asdf-vm/asdf-ruby);
3. After that you must add the 3.1.2 ruby to your computer with:

```
asdf install ruby 3.1.2
```

4. With that set, go to the root folder of the project and run `bundle install`;
5. Lastly, run the migrations with `rails db:migrate` and bring up the server with `rails s`;
6. It will be available at `http://localhost:3000`.

## Tests

The tests framework used is RSpec. In order to run the tests, if you are in the docker container, after raising it, you may run:

```
docker-compose exec -e RAILS_ENV=test app rspec
```

And if you are running it locally, you may run just `rspec`.

To run a specific file just add the file relative path to the command.

## Debug

To debug the app, add the following line to the breakpoint you need:

```
require 'debug'; binding.b
```

And then run the test you want to debug.

## Linter

The linter used in this project is [Rubocop](https://github.com/rubocop/rubocop-rails).

To run it in the docker container, after raising it, just run:

```
docker-compose exec app rubocop
```

Or locally, run `rubocop`.

### Extras

If you want to access the container via bash, just run:

```
docker-compose exec app bash
```

Then, the tests and lint can be ran directly, just remember to use `RAILS_ENV=test` before rspec

## Routes

The available routes are:

- `/` which redirects to:
- `/file_inputs` which is the index for all files inserted;
- `/file_inputs/new` which is where we can insert new purchaes via `.tab` files;
- `/file_inputs/:id` which shows a single inserted file with its purchases;
- `DELETE /file_inputs/:id` which deletes a inserted file with all its purchases along with it.
