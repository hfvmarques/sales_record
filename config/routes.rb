Rails.application.routes.draw do
  resources :file_inputs
  resources :file_inputs, only: [:index, :new, :create, :show]

  root "file_inputs#index"
end
