# frozen_string_literal: true

require 'rails_helper'

RSpec.describe FileInput, type: :model do
  let(:file) { File.open('spec/fixtures/example_input.tab') }
  let(:file_input) { create(:file_input) }

  it { expect(file_input).to be_valid }
  it { expect(file_input).to be_a_instance_of(FileInput) }

  describe '#process_purchases' do
    let(:process_purchases) { FileInput.process_purchases(file, file_input.id) }

    it { expect { process_purchases }.to change(Purchase, :count).by(5) }
    it { expect { process_purchases }.to change(Purchaser, :count).by(4) }
    it { expect { process_purchases }.to change(Merchant, :count).by(3) }
    it { expect { process_purchases }.to change(Item, :count).by(4) }
  end

  describe '#gross_income' do
    before { create(:purchase, file_input:) }

    it { expect(file_input.gross_income).to eq(49.36) }
  end

  describe '#set_params' do
    let(:data) { ['Snake Plissken',	'Cool Sneakers',	'5.0',	'4',	'123 Fake St',	'Sneaker Store Emporium'] }
    let(:parsed_data) do
      {
        purchaser_name: 'Snake Plissken',
        item_description: 'Cool Sneakers',
        item_price: 5.0,
        purchase_count: 4,
        merchant_address: '123 Fake St',
        merchant_name: 'Sneaker Store Emporium',
        file_input_id: 1
      }
    end

    it { expect(FileInput.send(:set_params, data, file_input.id)).to eq(parsed_data) }
  end
end
