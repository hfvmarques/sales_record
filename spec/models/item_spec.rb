# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Item, type: :model do
  let(:item) { build(:item, description:, price_in_cents:) }
  let(:description) { 'Item description' }
  let(:price_in_cents) { 1234 }

  it { expect(item).to be_valid }
  it { expect(item.price).to eq 12.34 }
  it { expect(item).to be_a_instance_of(Item) }

  context 'when the description is nil' do
    let(:description) { nil }

    it { expect(item).not_to be_valid }
  end

  context 'when the description is blank' do
    let(:description) { '' }

    it { expect(item).not_to be_valid }
  end

  context 'when the description is an empty string' do
    let(:description) { ' ' }

    it { expect(item).not_to be_valid }
  end

  context 'when the price is nil' do
    let(:price_in_cents) { nil }

    it { expect(item).not_to be_valid }
  end

  context 'when the price is blank' do
    let(:price_in_cents) { '' }

    it { expect(item).not_to be_valid }
  end

  context 'when the price is an empty string' do
    let(:price_in_cents) { ' ' }

    it { expect(item).not_to be_valid }
  end

  describe '#find_by' do
    let!(:item) { create(:item) }

    it { expect(Item.find_by(description: 'Item description')).to eq(item) }
    it { expect(Item.find_by(description: 'Whatever description')).to be_nil }
  end

  describe '#find_or_create' do
    let(:params) do
      {
        purchaser_name: 'João Silva',
        item_description: 'Pepperoni Pizza Slice',
        item_price: 10.0,
        purchase_count: 2,
        merchant_address: '987 Fake St',
        merchant_name: "Bob's Pizza",
        file_input_id: 1
      }
    end

    it { expect { Item.find_or_create(params) }.to change(Item, :count).by(1) }

    context 'when there are two items with the same description but same prices' do
      let!(:item) { create(:item, description: 'Pepperoni Pizza Slice', price_in_cents: 1000) }

      it { expect { Item.find_or_create(params) }.not_to change(Item, :count) }
    end

    context 'when there is already and item with the same description but different prices' do
      before { create(:item, description: 'Pepperoni Pizza Slice', price_in_cents: 750) }

      it 'creates a different item' do
        expect { Item.find_or_create(params) }.to change(Item, :count).by(1)

        expect(Item.all.first.price_in_cents).to eq(750)
        expect(Item.all.second.price_in_cents).to eq(1000)
      end
    end
  end
end
