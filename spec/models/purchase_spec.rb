# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Purchase, type: :model do
  let(:purchaser) { create(:purchaser) }
  let(:item) { create(:item) }
  let(:merchant) { create(:merchant) }
  let!(:file_input) { create(:file_input) }
  let(:purchase) { build(:purchase, purchaser:, item:, count:, merchant:, file_input:) }
  let(:count) { 4 }
  let(:params) do
    {
      purchaser_name: 'João Silva',
      item_description: 'Pepperoni Pizza Slice',
      item_price: 10.0,
      purchase_count: 2,
      merchant_address: '987 Fake St',
      merchant_name: "Bob's Pizza",
      file_input_id: 1
    }
  end

  it { expect(purchase).to be_valid }
  it { expect(purchase).to be_a_instance_of(Purchase) }
  it { expect { Purchase.register(params) }.to change(Purchase, :count).by(1) }

  context 'when the purchaser is nil' do
    let(:purchaser) { nil }

    it { expect(purchase).not_to be_valid }
  end

  context 'when the item is nil' do
    let(:item) { nil }

    it { expect(purchase).not_to be_valid }
  end

  context 'when the merchant is nil' do
    let(:merchant) { nil }

    it { expect(purchase).not_to be_valid }
  end

  context 'when the count is nil' do
    let(:count) { nil }

    it { expect(purchase).not_to be_valid }
  end

  context 'when the count is blank' do
    let(:count) { '' }

    it { expect(purchase).not_to be_valid }
  end

  context 'when the count is an empty string' do
    let(:count) { ' ' }

    it { expect(purchase).not_to be_valid }
  end

  describe '#income' do
    let!(:purchase) { create(:purchase) }

    it { expect(purchase.income).to eq(49.36) }
  end

  describe '#total_gross_income' do
    before { create_list(:purchase, 5) }

    it { expect(Purchase.total_gross_income).to eq(246.8) }
  end
end
