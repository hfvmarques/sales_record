# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Merchant, type: :model do
  let(:merchant) { build(:merchant, name:, address:) }
  let(:name) { 'Merchant Name' }
  let(:address) { '123 Some St' }
  let(:params) do
    {
      purchaser_name: 'João Silva',
      item_description: 'Pepperoni Pizza Slice',
      item_price: 10.0,
      purchase_count: 2,
      merchant_address: '987 Fake St',
      merchant_name: "Bob's Pizza",
      file_input_id: 1
    }
  end

  it { expect(merchant).to be_valid }
  it { expect(merchant).to be_a_instance_of(Merchant) }

  context 'when the name is nil' do
    let(:name) { nil }

    it { expect(merchant).not_to be_valid }
  end

  context 'when the name is blank' do
    let(:name) { '' }

    it { expect(merchant).not_to be_valid }
  end

  context 'when the name is an empty string' do
    let(:name) { ' ' }

    it { expect(merchant).not_to be_valid }
  end

  context 'when the address is nil' do
    let(:address) { nil }

    it { expect(merchant).not_to be_valid }
  end

  context 'when the address is blank' do
    let(:address) { '' }

    it { expect(merchant).not_to be_valid }
  end

  context 'when the address is an empty string' do
    let(:address) { ' ' }

    it { expect(merchant).not_to be_valid }
  end

  describe '#find_by' do
    let!(:merchant) { create(:merchant) }

    it { expect(Merchant.find_by(name: 'Merchant Name')).to eq(merchant) }
    it { expect(Merchant.find_by(name: 'Whatever Name')).to be_nil }
  end

  describe '#find_or_create' do
    it { expect { Merchant.find_or_create(params) }.to change(Merchant, :count).by(1) }

    context 'when there is already a merchant with the same name' do
      before { create(:merchant, name: "Bob's Pizza") }

      it { expect { Merchant.find_or_create(params) }.not_to change(Merchant, :count) }
    end
  end
end
