# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Purchaser, type: :model do
  let(:purchaser) { build(:purchaser, name:) }
  let(:name) { 'Purchaser Name' }

  it { expect(purchaser).to be_valid }
  it { expect(purchaser).to be_a_instance_of(Purchaser) }

  context 'when the name is nil' do
    let(:name) { nil }

    it { expect(purchaser).not_to be_valid }
  end

  context 'when the name is blank' do
    let(:name) { '' }

    it { expect(purchaser).not_to be_valid }
  end

  context 'when the name is an empty string' do
    let(:name) { ' ' }

    it { expect(purchaser).not_to be_valid }
  end

  describe '#find_by_name' do
    let!(:purchaser) { create(:purchaser) }

    it { expect(Purchaser.find_by(name: 'Purchaser Name')).to eq(purchaser) }
    it { expect(Purchaser.find_by(name: 'Whatever Name')).to be_nil }
  end

  describe '#find_or_create' do
    let(:params) do
      {
        purchaser_name: 'João Silva',
        item_description: 'Pepperoni Pizza Slice',
        item_price: 10.0,
        purchase_count: 2,
        merchant_address: '987 Fake St',
        merchant_name: "Bob's Pizza",
        file_input_id: 1
      }
    end

    it { expect { Purchaser.find_or_create(params) }.to change(Purchaser, :count).by(1) }

    context 'when there is already a purchaser with the same name' do
      before { create(:purchaser, name: 'João Silva') }

      it { expect { Purchaser.find_or_create(params) }.not_to change(Purchaser, :count) }
    end
  end
end
