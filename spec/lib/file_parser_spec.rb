# frozen_string_literal: true

require 'rails_helper'
require_relative './../../lib/file_parser'

describe FileParser do
  let(:file) { File.open('spec/fixtures/example_input.tab') }
  let(:inputed_file) { FileParser.new(file) }
  let(:headers) do
    [
      'purchaser name',
      'item description',
      'item price',
      'purchase count',
      'merchant address',
      'merchant name'
    ]
  end

  let(:parsed_file) do
    [
      ['João Silva',	'Pepperoni Pizza Slice',	'10.0',	'2',	'987 Fake St',	"Bob's Pizza"],
      ['Amy Pond',	'Cute T-Shirt',	'10.0',	'5',	'456 Unreal Rd',	"Tom's Awesome Shop"],
      ['Marty McFly',	'Cool Sneakers',	'5.0',	'1',	'123 Fake St',	'Sneaker Store Emporium'],
      ['Snake Plissken',	'Cool Sneakers',	'5.0',	'4',	'123 Fake St',	'Sneaker Store Emporium'],
      ['João Silva',	'Cute T-Shirt',	'7.95',	'1',	'456 Unreal Rd',	"Tom's Awesome Shop"]
    ]
  end

  it { expect(inputed_file).to be_a_instance_of(FileParser) }
  it { expect(inputed_file.headers).to eq(headers) }
  it { expect(inputed_file.content).to eq(parsed_file) }
end
