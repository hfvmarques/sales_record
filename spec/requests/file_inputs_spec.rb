# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/file_inputs', type: :request do
  let(:valid_attributes) { {} }

  before { create(:file_input) }

  describe 'GET /index' do
    it 'renders a successful response' do
      FileInput.create! valid_attributes
      get file_inputs_url
      expect(response).to be_successful
    end
  end

  describe 'GET /show' do
    it 'renders a successful response' do
      file_input = FileInput.create! valid_attributes
      get file_input_url(file_input)
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_file_input_url
      expect(response).to be_successful
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested file_input' do
      file_input = FileInput.create! valid_attributes
      expect { delete file_input_url(file_input) }.to change(FileInput, :count).by(-1)
    end

    it 'redirects to the file_inputs list' do
      file_input = FileInput.create! valid_attributes
      delete file_input_url(file_input)
      expect(response).to redirect_to(file_inputs_url)
    end
  end
end
