# frozen_string_literal: true

FactoryBot.define do
  factory :item do
    description { 'Item description' }
    price_in_cents { 1234 }
  end
end
