# frozen_string_literal: true

FactoryBot.define do
  factory :purchase do
    purchaser { create(:purchaser) }
    item { create(:item) }
    merchant { create(:merchant) }
    count { 4 }
    file_input { create(:file_input) }
  end
end
