# frozen_string_literal: true

FactoryBot.define do
  factory :purchaser do
    name { 'Purchaser Name' }
  end
end
