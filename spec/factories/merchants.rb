# frozen_string_literal: true

FactoryBot.define do
  factory :merchant do
    name { 'Merchant Name' }
    address { '123 Some St' }
  end
end
