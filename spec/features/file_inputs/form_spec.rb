# frozen_string_literal: true

require 'rails_helper'

describe 'input new file', type: :feature do
  before do
    visit '/file_inputs/new'
  end

  it 'creates a new file_input' do
    visit '/file_inputs/new'
    find('form input[type="file"]').set('spec/fixtures/example_input.tab')
    click_button 'Create File input'

    expect(page).to have_content 'success'
  end
end
