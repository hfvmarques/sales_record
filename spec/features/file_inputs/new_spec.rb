# frozen_string_literal: true

require 'rails_helper'

describe 'new input file page', type: :feature do
  before do
    visit '/file_inputs/new'
  end

  it { expect(page).to have_content 'Insert new file' }
  it { expect(page).to have_content 'Back' }
end
