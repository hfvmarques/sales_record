# frozen_string_literal: true

require 'rails_helper'

describe 'show file inputs page', type: :feature do
  before do
    create(:file_input)

    visit '/file_inputs'
  end

  it { expect(page).to have_content 'Inserted Files' }
  it { expect(page).to have_content 'Gross Income' }
  it { expect(page).to have_content 'Total gross income' }
  it { expect(page).to have_content 'Action' }
  it { expect(page).to have_content Time.current.strftime('%d/%m/%Y %H:%M UTC') }
  it { expect(page).to have_content 'Show' }
  it { expect(page).to have_content 'Insert new file' }
end
