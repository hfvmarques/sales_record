# frozen_string_literal: true

require 'rails_helper'

describe 'show file input page', type: :feature do
  before do
    create(:file_input)

    visit '/file_inputs/1'
  end

  it { expect(page).to have_content 'Back' }
  it { expect(page).to have_content 'Destroy' }

  it 'destroys file inputs' do
    click_button 'Destroy'

    expect(page).to have_content 'File was successfully destroyed.'
  end
end
