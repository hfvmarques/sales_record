# frozen_string_literal: true

require 'rails_helper'

describe 'file input form', type: :feature do
  before do
    create(:file_input)

    visit '/file_inputs/1'
  end

  it { expect(page).to have_content 'Purchaser' }
  it { expect(page).to have_content 'Item Description' }
  it { expect(page).to have_content 'Item Price' }
  it { expect(page).to have_content 'Count' }
  it { expect(page).to have_content 'Merchant Name' }
  it { expect(page).to have_content 'Merchant Address' }
  it { expect(page).to have_content 'Gross income from this file:' }
  it { expect(page).to have_content 'Total gross income:' }
end
