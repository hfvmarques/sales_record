# frozen_string_literal: true

require 'rails_helper'

RSpec.describe FileInputsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/file_inputs').to route_to('file_inputs#index')
    end

    it 'routes to #new' do
      expect(get: '/file_inputs/new').to route_to('file_inputs#new')
    end

    it 'routes to #show' do
      expect(get: '/file_inputs/1').to route_to('file_inputs#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/file_inputs/1/edit').to route_to('file_inputs#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/file_inputs').to route_to('file_inputs#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/file_inputs/1').to route_to('file_inputs#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/file_inputs/1').to route_to('file_inputs#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/file_inputs/1').to route_to('file_inputs#destroy', id: '1')
    end
  end
end
